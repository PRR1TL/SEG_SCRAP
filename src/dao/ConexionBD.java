/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author PRR1TL
 */
public class ConexionBD {
    protected Connection conexion;
    private String dbName = "DB_SystemKPI_SQL";
    private String serverName = "SGLERSQL01";
    private String serverPort = "1433";
    private String userName = "USRKPI";
    private String password = "nZQI5NZZN8smsSsc";

    public Connection getConexion() {
        return conexion;
    }
    
    //DRIVER JDBC ACCESS
    //private final String driverJDBC = "net.ucanaccess.jdbc.UcanaccessDriver";
    
    //DRIVER JDBC SQLSERVER
    private final String driverJDBCSQLServer = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    
    //URL SERVIDOR SQLSERVER - SEG
    private final String urlDB = "jdbc:sqlserver://"+serverName+"\\SQLEXPRESS:"+serverPort+";databaseName="+dbName+";user="+userName+";password="+password+";";
    
    public void conectar() throws Exception {
        try {
            Class.forName(driverJDBCSQLServer);
            conexion = DriverManager.getConnection(urlDB);
        } catch (ClassNotFoundException | SQLException e) {
            throw e;
        }
    }

    public void cerrar() throws Exception {
        try {
            if (conexion != null) {
                if (!conexion.isClosed()) {
                    conexion.close();
                }
            }
        } catch (SQLException e) {
            throw e;
        }
    }   
    
}
