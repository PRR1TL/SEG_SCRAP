/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import javax.swing.JOptionPane;

/**
 *
 * @author PRR1TL
 */
public class DAOScrap extends ConexionBD {
    private ArrayList listaRegistros;
    private PreparedStatement ps, ps2, ps3, ps4;
    private ResultSet rs, rs2, rs3, rs4;
    
    String sequenNumber = "";
    
    //QUERYS PARA LA INSERCION DE SCRAP
    String INSERT_IFK_DIA = "INSERT INTO IfkDaily (doc, costCenter, profitCenter, material, materialDescription, reason, reasonAdd, docDate, docTime, userName, docHeader, operation, scrapQuantity, unitAssembly, valueCompanyCode, companyCodeCurrency, docCategory) VALUES (?, ?, ?, ?, ?, ?, ?, CAST(CONVERT(DATETIME, ?, 104) as DATETIME), ?, ?, ?, ?, ?, ?, ?, ?, ?);";
    String INSERT_MISCELLANIOUS_DIA = "INSERT INTO MiscellaneousDaily (sequenNumber, costElement, costCenter, costDoc, profitCenter, material, materialDescription, userName, docDate, reasonAdd, reason, createdOn, componentQuantity, unitMeasure, valueCompanyCode, companyCodeCurrency, periodMis, docTime) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, CAST(CONVERT(DATETIME, ?, 104) as DATETIME), ?, ?, CAST(CONVERT(DATETIME, ?, 104) as DATETIME), ?, ?, ?, ?, ?, ?);";
    
    String DELETE_IFK_MES = "DELETE FROM IfkDaily WHERE DATEPART(YEAR, docDate) = ? AND DATEPART(MONTH, docDate) = ? ; ";
    String DELETE_MICELLANIOUS_MES = "DELETE FROM MiscellaneousDaily WHERE DATEPART(YEAR, docDate) = ? AND DATEPART(MONTH, docDate) = ? ; ";
    
    //SEGUIMIENTO DE REGISTRO DE ARCHIVOS DE SCRAP
    String INSERT_REGISTRO_ARCHIVO = "INSERT INTO HistoricoDocScrap (fecha, archivo, tipo) VALUES(GETDATE() , ?, ?); ";
    String DELETE_REGISTRO_ARCHIVO = "DELETE FROM HistoricoDocScrap WHERE tipo = ? ;";
    String CONSULTA_REGISTROS_ARCHIVOS = "SELECT archivo FROM HistoricoDocScrap; ";
    
    
    public void insertaMicellaniousDay(ArrayList registro) throws Exception { 
        Object[] reg = new Object[registro.size()]; 
        
        try { 
            this.conectar(); 
            for (int i = 0; i < registro.size(); i++) { 
                reg[i] = (Object) registro.get(i); 
            } 
            
            /* CONVERSION DE FECHA Y DECREMENTO DE LA MISMA */ 
            String untildate = reg[8].toString();//can take any date in current format            
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");  
            Calendar cal = Calendar.getInstance(); 
            cal.setTime( dateFormat.parse(untildate)); 
            cal.add( Calendar.DATE, -1 ); 
            String convertedDate = dateFormat.format(cal.getTime()); 
            
            /* CONVERSION DE FECHA A STRING */
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy"); 
            String dateInString = convertedDate.replace(".", "/"); 
            Date date = formatter.parse(dateInString); 
            SimpleDateFormat formatoInglesEU = new SimpleDateFormat("EEE", new Locale("EN", "US")); 
            String caDay = formatoInglesEU.format(date);
            
            //COMPARACION DE DIAS PARA EVALUAR SI ES DOMINGO QUE LO MANDE A VIERNES 
            if (caDay.contains("Sun")){ 
                cal.setTime( dateFormat.parse(untildate)); 
                cal.add( Calendar.DATE, -3 ); 
                convertedDate = dateFormat.format(cal.getTime()); 
            } 
            
            System.out.println("dao.DAOScrap.insertaMicellaniousDay(): "+untildate+" -> "+convertedDate);
            
            /* INSERCION A LA BASE DE DATOS */ 
            ps = this.conexion.prepareStatement(INSERT_MISCELLANIOUS_DIA);
            sequenNumber = reg[0].toString();
            ps.setString(1, reg[0].toString());//sequenNumber 
            ps.setString(2, reg[1].toString());//costElement 
            ps.setString(3, reg[2].toString());//costCenter 
            ps.setString(4, reg[3].toString());//costDoc 
            ps.setString(5, reg[4].toString());//profitCenter 
            ps.setString(6, reg[5].toString());//material(numero de referencia) 
            ps.setString(7, reg[6].toString());//materialDescription 
            ps.setString(8, reg[7].toString());//userName 
            //ps.setString(9, reg[8].toString());//docDate 
            ps.setString(9, convertedDate);//docDate 
            ps.setString(10, reg[9].toString());//reason 
            ps.setString(11, reg[10].toString());//reasonAdd(Reason Description) 
            //ps.setString(12, reg[11].toString());//createdOn 
            ps.setString(12, convertedDate);//createdOn 
            ps.setString(13, reg[12].toString());//componentQuantity 
            ps.setString(14, reg[13].toString());//unitMeasure 
            ps.setString(15, reg[14].toString());//valueCompanyCode 
            ps.setString(16, reg[15].toString());//companyCodeCurrency 
            ps.setString(17, reg[16].toString());//periodMis 
            ps.setString(18, reg[17].toString());//docTime 
            
            ps.executeUpdate();
        } catch (Exception e) {            
            //throw e;
            JOptionPane.showMessageDialog(null, e + ": "+sequenNumber );
        } finally {
            ps.close();
            this.cerrar();
        }
    }
    
    public void insertaIFKDay(ArrayList registro) throws Exception { 
        Object[] reg = new Object[registro.size()];
        try {
            this.conectar();
            for (int i = 0; i < registro.size(); i++) { 
                reg[i] = (Object) registro.get(i); 
            } 
            
            /* CONVERSION DE FECHA Y DECREMENTO DE LA MISMA */
            String untildate = reg[7].toString();//can take any date in current format 
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy"); 
            Calendar cal = Calendar.getInstance(); 
            cal.setTime( dateFormat.parse(untildate)); 
            cal.add( Calendar.DATE, -1 ); 
            String convertedDate = dateFormat.format(cal.getTime()); 
            
            /* CONVERSION DE FECHA A STRING */
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy"); 
            String dateInString = convertedDate.replace(".", "/"); 
            Date date = formatter.parse(dateInString); 
            SimpleDateFormat formatoInglesEU = new SimpleDateFormat("EEE", new Locale("EN", "US")); 
            String caDay = formatoInglesEU.format(date);
            
            //COMPARACION DE DIAS PARA EVALUAR SI ES DOMINGO QUE LO MANDE A VIERNES 
            if (caDay.contains("Sun")){ 
                cal.setTime( dateFormat.parse(untildate)); 
                cal.add( Calendar.DATE, -3 ); 
                convertedDate = dateFormat.format(cal.getTime()); 
            } 
            
            System.out.println("dao.DAOScrap.insertaMicellaniousDay(): "+untildate+" -> "+convertedDate);
            
            
            /* INSERCION A LA BASE DE DATOS */
            ps = this.conexion.prepareStatement(INSERT_IFK_DIA);
            sequenNumber = reg[0].toString();
            ps.setString(1, reg[0].toString());//DOC
            ps.setString(2, reg[1].toString());//costElement
            ps.setString(3, reg[2].toString());//procCenter
            ps.setString(4, reg[3].toString());//material
            ps.setString(5, reg[4].toString());//materialDescription
            ps.setString(6, reg[5].toString());//reason
            ps.setString(7, reg[6].toString());//reasonAdd
            //ps.setString(8, reg[7].toString());//doctDate
            ps.setString(8, convertedDate);//doctDate
            ps.setString(9, reg[8].toString());//docTime
            ps.setString(10, reg[9].toString());//userName
            ps.setString(11, reg[10].toString());//txtCenter
            ps.setString(12, reg[11].toString());//operation
            ps.setString(13, reg[12].toString());//scraptQuantity
            ps.setString(14, reg[13].toString());//unitAssembly
            ps.setString(15, reg[14].toString());//valueCompanyCode
            ps.setString(16, reg[15].toString());//companyCodeCurrency
            ps.setString(17, reg[16].toString());//docCategory
            //ps.setString(18, reg[17].toString());//docTime
            
            ps.executeUpdate();
        } catch (Exception e) { 
            JOptionPane.showMessageDialog(null, e + ": "+sequenNumber );
        } finally {
            ps.close();
            this.cerrar();
        }
    }
    
    public void deleteMicellaniousMonth(String vMonth) throws Exception { 
        //System.out.println("dao.DAOScrap.deleteMicellaniousMonth(): "+vMonth);
        try { 
            this.conectar();
            /* CONVERSION DE FECHA Y DECREMENTO DE LA MISMA */ 
            SimpleDateFormat mFormat = new SimpleDateFormat("MM");  
            SimpleDateFormat yFormat = new SimpleDateFormat("yyyy");  
            Calendar cal = Calendar.getInstance(); 
            String mes = mFormat.format(cal.getTime()); 
            String anio = yFormat.format(cal.getTime()); 
            //System.out.println("dao.DAOScrap.insertaMicellaniousDay().convertDate: "+mes+" anio: "+anio );
            
            /* INSERCION A LA BASE DE DATOS */             
            ps = this.conexion.prepareStatement(DELETE_MICELLANIOUS_MES);
            ps.setString(1, anio);
            ps.setString(2, mes);

            ps.executeUpdate();
        } catch (Exception e) { 
            JOptionPane.showMessageDialog(null, e + ": "+sequenNumber );
        } finally {
            ps.close();
            this.cerrar();
        }
    } 
    
    public void deleteMicellaniousWeek(String vMonth) throws Exception { 
        //System.out.println("dao.DAOScrap.deleteMicellaniousMonth(): "+vMonth);
        try { 
            this.conectar();
            /* CONVERSION DE FECHA Y DECREMENTO DE LA MISMA */ 
            SimpleDateFormat mFormat = new SimpleDateFormat("MM");  
            SimpleDateFormat yFormat = new SimpleDateFormat("yyyy");  
            Calendar cal = Calendar.getInstance(); 
            String mes = mFormat.format(cal.getTime()); 
            String anio = yFormat.format(cal.getTime()); 
            //System.out.println("dao.DAOScrap.insertaMicellaniousDay().convertDate: "+mes+" anio: "+anio );
            
            /* INSERCION A LA BASE DE DATOS */             
            ps = this.conexion.prepareStatement(DELETE_MICELLANIOUS_MES); 
            ps.setString(1, anio); 
            ps.setString(2, mes); 

            ps.executeUpdate(); 
        } catch (Exception e) { 
            JOptionPane.showMessageDialog(null, e + ": "+sequenNumber );
        } finally {
            ps.close();
            this.cerrar();
        }
    }
    
    public void deleteIFKMonth(String vMonth) throws Exception { 
        //System.out.println("dao.DAOScrap.deleteIFKMonth(): "+vMonth);
        try {
            this.conectar();
            /* CONVERSION DE FECHA Y DECREMENTO DE LA MISMA */ 
            SimpleDateFormat mFormat = new SimpleDateFormat("MM");  
            SimpleDateFormat yFormat = new SimpleDateFormat("yyyy");  
            Calendar cal = Calendar.getInstance(); 
            String mes = mFormat.format(cal.getTime()); 
            String anio = yFormat.format(cal.getTime()); 
            System.out.println("dao.DAOScrap.deleteIFKMoth().convertDate: "+mes+" anio: "+anio ); 
            
            /* INSERCION A LA BASE DE DATOS */             
            ps = this.conexion.prepareStatement(DELETE_IFK_MES); 
            ps.setString(1, anio); 
            ps.setString(2, mes); 
            
            ps.executeUpdate(); 
        } catch (Exception e) { 
            JOptionPane.showMessageDialog(null, e + ": "+sequenNumber ); 
        } finally { 
            ps.close(); 
            this.cerrar(); 
        } 
    } 
    
    public void deleteIFKWeek(String vMonth) throws Exception { 
        //System.out.println("dao.DAOScrap.deleteIFKMonth(): "+vMonth);
        try {
            this.conectar();
            /* CONVERSION DE FECHA Y DECREMENTO DE LA MISMA */ 
            SimpleDateFormat mFormat = new SimpleDateFormat("MM");  
            SimpleDateFormat yFormat = new SimpleDateFormat("yyyy");  
            Calendar cal = Calendar.getInstance(); 
            String mes = mFormat.format(cal.getTime()); 
            String anio = yFormat.format(cal.getTime()); 
            System.out.println("dao.DAOScrap.deleteIFKWeek().convertDate: "+mes+" anio: "+anio );
            
            /* INSERCION A LA BASE DE DATOS */             
            ps = this.conexion.prepareStatement(DELETE_IFK_MES); 
            ps.setString(1, anio); 
            ps.setString(2, mes); 
            
            ps.executeUpdate(); 
        } catch (Exception e) { 
            JOptionPane.showMessageDialog(null, e + ": "+sequenNumber );
        } finally {
            ps.close();
            this.cerrar();
        }
    }
    
    public void registroArchivo(String archivo, int tipo) throws Exception { 
        try {
            this.conectar(); 
           
            /* INSERCION A LA BASE DE DATOS */ 
            ps = this.conexion.prepareStatement(INSERT_REGISTRO_ARCHIVO); 
            ps.setString(1, archivo); 
            ps.setInt(2, tipo); 
            
            ps.executeUpdate();
        } catch (Exception e) { 
            JOptionPane.showMessageDialog(null, e );
        } finally {
            ps.close();
            this.cerrar();
        }
    }
    
    public void elimnaRegistroArchivo(int tipo) throws Exception { 
        try {
            this.conectar(); 
           
            /* INSERCION A LA BASE DE DATOS */ 
            ps = this.conexion.prepareStatement(DELETE_REGISTRO_ARCHIVO);
            ps.setInt(1, tipo);
            
            ps.executeUpdate();
        } catch (Exception e) { 
            JOptionPane.showMessageDialog(null, e );
        } finally { 
            ps.close();
            this.cerrar();
        }
    } 
    
    public ArrayList listaRegistrosArchivo() throws Exception { 
        ArrayList reg = new ArrayList();
        try {
            this.conectar(); 
           
            /* INSERCION A LA BASE DE DATOS */ 
            ps = this.conexion.prepareStatement(CONSULTA_REGISTROS_ARCHIVOS);            
            rs = ps.executeQuery();
            
            while (rs.next()) {
                reg.add(rs.getString(1));
            }             
            
        } catch (Exception e) { 
            JOptionPane.showMessageDialog(null, e );
        } finally {
            ps.close();
            this.cerrar();
        }
        return reg;
    }
    
    
    
}
