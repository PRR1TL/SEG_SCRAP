/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package limpiartxt;

import java.text.ParseException;
import java.text.SimpleDateFormat; 
import java.util.Calendar;
import java.util.Date;
import java.util.Locale; 
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author PRR1TL
 */
public class pruebaFecha {
    
    public static void main(String[] args) throws ParseException { 
        
        //SEMANA 
        String fecha = "28.07.2019"; 
        String fIni = ""; 
        String fFin = ""; 
        /*
            S -> 0
            D -> 1
            L -> 2
            M -> 3
            X -> 4
            J -> 5
            V -> 6            
        */

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy"); 
        Calendar cal = Calendar.getInstance(); 
        Calendar fI = Calendar.getInstance(); 
        Calendar fF = Calendar.getInstance(); 
        cal.setTime( dateFormat.parse(fecha)); 
        fI.setTime( dateFormat.parse(fecha)); 
        fF.setTime( dateFormat.parse(fecha)); 
        int semanaAnno = cal.get(Calendar.WEEK_OF_YEAR); 
        int diaSemana = cal.get(Calendar.DAY_OF_WEEK); 

        System.out.println("main(): "+dateFormat.format(fI.getTime())+" - "+ dateFormat.format(fF.getTime()));
        if (diaSemana > 2) { 
            System.out.println("+"); 
            fI.add( Calendar.DATE, (diaSemana-2) ); 
            fF.add( Calendar.DATE, (7-diaSemana) ); 
        } else { 
            if(diaSemana == 0 ){ 
                System.out.println("*"); 
                fF.add( Calendar.DATE, (8-diaSemana) ); 
            } else if(diaSemana == 1 ) { 
                fI.add( Calendar.DATE, (diaSemana-3) ); 
                fF.add( Calendar.DATE, (8-diaSemana) ); 
            } else if (diaSemana == 2 ) { 
                fI.add( Calendar.DATE, (diaSemana-2) ); 
                fF.add( Calendar.DATE, (7-diaSemana) ); 
            } 
        } 
        
        fIni = dateFormat.format(fI.getTime()); 
        fFin = dateFormat.format(fF.getTime()); 
        
        System.out.println("salidad ("+diaSemana+"); "+fIni+" , "+fFin);
            
    } 
        
        /* PARA DOMINGOS */ 
//        try {
//            String untildate = "29.07.2019";//can take any date in current format
//            SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy"); 
//            Calendar cal = Calendar.getInstance(); 
//            cal.setTime( dateFormat.parse(untildate)); 
//            cal.add( Calendar.DATE, -1 ); 
//            String convertedDate = dateFormat.format(cal.getTime()); 
//            
//            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy"); 
//            String dateInString = convertedDate.replace(".", "/"); 
//            
//            Date date = formatter.parse(dateInString); 
//            SimpleDateFormat formatoInglesEU = new SimpleDateFormat("EEE", new Locale("EN", "US")); 
//            String caDay = formatoInglesEU.format(date); 
//            if (caDay.contains("Sun")){ 
//                System.out.println("entra: "+formatoInglesEU.format(date)); 
//                cal.setTime( dateFormat.parse(untildate)); 
//                cal.add( Calendar.DATE, -3 ); 
//                convertedDate = dateFormat.format(cal.getTime()); 
//            } 
//            
//            System.out.println("limpiartxt.pruebaFecha.main()"+convertedDate);   
//        } catch (ParseException ex) {
//            Logger.getLogger(pruebaFecha.class.getName()).log(Level.SEVERE, null, ex);
//        }
    
}
