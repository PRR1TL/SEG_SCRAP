/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
    INDICADOR 1: MISCELANEO
    INDICADOR 2: IFK
 */
package limpiartxt;

import java.io.*;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author GUR5TL
 */
public class LeerFichero {

    public static ArrayList ContenidoArchivos(File directorio) throws ParseException, Exception { 
        File[] files = directorio.listFiles();    
        ArrayList dir = new ArrayList();
        
        BasicFileAttributes attrs;
        int cDM = 0;
        int cDI = 0;
        int cMM = 0;
        int cMI = 0;
        Date dateDM = null;
        Date dateDI = null;
        Date dateMM = null;
        Date dateMI = null;
        
        String aDM = null; 
        String aDI = null; 
        String aMM = null; 
        String aMI = null; 
        
        //CONSULTA DE REGISTROS
        ArrayList list = new ArrayList();         
        list = new dao.DAOScrap().listaRegistrosArchivo();
        
        try {
            for (File file : files) { 
                if (!file.isDirectory()) { 
                    if (file.getCanonicalPath().contains("RFCOREPOR_D1") || file.getCanonicalPath().contains("RFCOREPOR_D2") //dayli
                     || file.getCanonicalPath().contains("RFCOREPOR_M1") || file.getCanonicalPath().contains("RFCOREPOR_M2")) { //moth
                        
                        if (file.getCanonicalPath().contains("RFCOREPOR_D1")){ 
                            attrs = Files.readAttributes(file.toPath(), BasicFileAttributes.class); 
                            FileTime time = attrs.creationTime(); 
                            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
                            String dateCreated = df.format(time.toMillis()); 
                            Date valD = df.parse(dateCreated); 
                            
                            if (cDM > 0){ 
                                if (valD.after(dateDM)){                                    
                                    aDM = file.getCanonicalPath();
                                } 
                                
                                attrs = Files.readAttributes(file.toPath(), BasicFileAttributes.class); 
                                FileTime time2 = attrs.creationTime(); 
                                String auxD2 = df.format(time2.toMillis()); 
                                dateDM = df.parse(auxD2);  
                            } else {
                                attrs = Files.readAttributes(file.toPath(), BasicFileAttributes.class); 
                                FileTime time2 = attrs.creationTime(); 
                                String auxD1 = df.format(time2.toMillis()); 
                                dateDM = df.parse(auxD1); 
                                aDM = file.getCanonicalPath();                              
                            }  
                            cDM++;  
                        } else if (file.getCanonicalPath().contains("RFCOREPOR_D2")){ 
                            attrs = Files.readAttributes(file.toPath(), BasicFileAttributes.class); 
                            FileTime time = attrs.creationTime(); 
                            // CONVERSION DE TIPOS PARA LA FECHA DEL ARCHIVO 
                            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
                            String dateCreated = df.format(time.toMillis()); 
                            Date valD = df.parse(dateCreated); 
                            
                            if (cDI > 0){ 
                                //COMPARACION PARA ARCHIVO MÁS RECIENTE 
                                if (valD.after(dateDI)){ 
                                    aDI = file.getCanonicalPath(); 
                                } 
                                
                                attrs = Files.readAttributes(file.toPath(), BasicFileAttributes.class); 
                                FileTime time2 = attrs.creationTime(); 
                                String auxD2 = df.format(time2.toMillis()); 
                                dateDI = df.parse(auxD2); 
                            } else {
                                attrs = Files.readAttributes(file.toPath(), BasicFileAttributes.class); 
                                FileTime time2 = attrs.creationTime(); 
                                String auxD2 = df.format(time2.toMillis()); 
                                dateDI = df.parse(auxD2); 
                                aDI = file.getCanonicalPath(); 
                            }
                            
                            cDI++; 
                        } else if (file.getCanonicalPath().contains("RFCOREPOR_M1")){ 
                            attrs = Files.readAttributes(file.toPath(), BasicFileAttributes.class); 
                            FileTime time = attrs.creationTime(); 
                            // CONVERSION DE TIPOS PARA LA FECHA DEL ARCHIVO 
                            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
                            String dateCreated = df.format(time.toMillis()); 
                            Date valD = df.parse(dateCreated); 
                            
                            if (cMM > 0){ 
                                //COMPARACION PARA ARCHIVO MÁS RECIENTE 
                                if (valD.after(dateDI)){ 
                                    aMM = file.getCanonicalPath(); 
                                } 
                                
                                attrs = Files.readAttributes(file.toPath(), BasicFileAttributes.class); 
                                FileTime time2 = attrs.creationTime(); 
                                String auxD2 = df.format(time2.toMillis()); 
                                dateMM = df.parse(auxD2); 
                            } else {
                                attrs = Files.readAttributes(file.toPath(), BasicFileAttributes.class); 
                                FileTime time2 = attrs.creationTime(); 
                                String auxD2 = df.format(time2.toMillis()); 
                                dateMM = df.parse(auxD2); 
                                aMM = file.getCanonicalPath(); 
                            }
                            
                            cMM++; 
                        } else if (file.getCanonicalPath().contains("RFCOREPOR_M2")){ 
                            attrs = Files.readAttributes(file.toPath(), BasicFileAttributes.class); 
                            FileTime time = attrs.creationTime(); 
                            // CONVERSION DE TIPOS PARA LA FECHA DEL ARCHIVO 
                            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
                            String dateCreated = df.format(time.toMillis()); 
                            Date valD = df.parse(dateCreated); 
                            
                            if (cMI > 0){ 
                                //COMPARACION PARA ARCHIVO MÁS RECIENTE 
                                if (valD.after(dateDI)){ 
                                    aMI = file.getCanonicalPath(); 
                                } 
                                
                                attrs = Files.readAttributes(file.toPath(), BasicFileAttributes.class); 
                                FileTime time2 = attrs.creationTime(); 
                                String auxD2 = df.format(time2.toMillis()); 
                                dateMI = df.parse(auxD2); 
                            } else {
                                attrs = Files.readAttributes(file.toPath(), BasicFileAttributes.class); 
                                FileTime time2 = attrs.creationTime(); 
                                String auxD2 = df.format(time2.toMillis()); 
                                dateMI = df.parse(auxD2); 
                                aMI = file.getCanonicalPath(); 
                            }                             
                            cMI++; 
                        }                         
                    } 
                } 
            }
            //LLENADO DE ARRAY 
            if (aDM != null){ 
                //COMPROBACION DE ARCHIVO QUE NO EXISTA EN LA BASE DE DATOS
                int bnExist = 0;
                if (list.size() > 0){ 
                    for (int i = 0; i < list.size(); i++){
                        if (aDM.contains(list.get(i).toString())){
                           bnExist = 1;
                        } 
                    } 
                    
                    if (bnExist == 0){
                        dir.add(aDM); 
                    } else {
                        JOptionPane.showMessageDialog(null, "EL ARCHIVO YA HA SIDO CARGADO ANTERIORMENTE");
                    }
                } else {
                    dir.add(aDM);
                }
            } 
            if (aDI != null){ 
                //COMPROBACION DE ARCHIVO QUE NO EXISTA EN LA BASE DE DATOS
                int bnExist = 0;
                if (list.size() > 0){ 
                    for (int i = 0; i < list.size(); i++){
                        if (aDI.contains(list.get(i).toString())){
                            //CONQUE SEA IGUAL A UN REGISTRO >_PELEISHON_<
                            bnExist = 1;
                        } 
                    } 
                    
                    if (bnExist == 0){
                        dir.add(aDI); 
                    } else {
                        JOptionPane.showMessageDialog(null, "EL ARCHIVO YA HA SIDO CARGADO ANTERIORMENTE");
                    }
                } else {
                    dir.add(aDI);
                }
            } 
            if (aMM != null){ 
                //COMPROBACION DE ARCHIVO QUE NO EXISTA EN LA BASE DE DATOS
                int bnExist = 0;
                if (list.size() > 0) { 
                    for (int i = 0; i < list.size(); i++){ 
                        if (aMM.contains(list.get(i).toString())){ 
                            //CONQUE SEA IGUAL A UN REGISTRO NO PASA 
                            bnExist = 1;
                        } 
                    } 
                    
                    if (bnExist == 0){
                        dir.add(aMM); 
                    } else {
                        JOptionPane.showMessageDialog(null, "EL ARCHIVO YA HA SIDO CARGADO ANTERIORMENTE");
                    }
                } else { 
                    dir.add(aMM);
                } 
            } 
            if (aMI != null){ 
                //COMPROBACION DE ARCHIVO QUE NO EXISTA EN LA BASE DE DATOS 
                int bnExist = 0; 
                if (list.size() > 0){ 
                    for (int i = 0; i < list.size(); i++){ 
                        if (aMI.contains(list.get(i).toString())){ 
                            //CONQUE SEA IGUAL A UN REGISTRO NO PASA 
                            bnExist = 1;
                        } 
                    } 
                    
                    if (bnExist == 0){
                        dir.add(aMM); 
                    } else {
                        JOptionPane.showMessageDialog(null, "EL ARCHIVO YA HA SIDO CARGADO ANTERIORMENTE");
                    }
                } else {
                    dir.add(aMI);
                }  
            }             
        } catch (IOException e) { 
            e.printStackTrace(); 
        } 
        return dir; 
    } 

    public static void analisisArchivo() throws ParseException, Exception{ 
        ArrayList reg;
        
        File Directorio = new File("\\\\Sglerigc01\\ilm\\Dept\\MOE1\\Shared\\Informacion_general\\proyectosSG\\ProyectoOEE\\Bitacora\\Scrap\\");
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        int contador = 0;
        int bnRemove = 0;
        int tipo = 0;
        String[] parts;
        ArrayList ub;
        String linea; 
        
        ub = LeerFichero.ContenidoArchivos(Directorio); 
        for (int i = 0; i < ub.size(); i++) { 
            System.out.println("\nlimpiartxt.LeerFichero.analisisArchivo().UB: "+ub.get(i)); 
            if (ub.get(i) != null) {
                try {
                    archivo = new File((String) ub.get(i));
                    fr = new FileReader(archivo);
                    br = new BufferedReader(fr);
                    linea = br.readLine();
                    while ((linea = br.readLine()) != null) {
                        linea = linea.replace(" ", "");
                        if (!linea.startsWith("*")) {
                            if (linea.startsWith("SP") & contador <= 1) {
                                contador = contador + 1; 
                            } else if (contador == 2) { 
                                if(linea.startsWith("LD|-*") ){ 
                                    break; 
                                } else if (linea.startsWith("LDR") || linea.startsWith("EL") || linea.startsWith("LDR") || linea.startsWith("LDC")
                                        || linea.startsWith("LDT") || linea.startsWith("LDF") || linea.startsWith("SP") || linea.startsWith("LD-") || linea.startsWith("EP")
                                        //|| linea.startsWith("LD|S") || linea.startsWith("LD|C") || linea.startsWith("LD|Q") || linea.startsWith("LD|R") 
                                        //|| linea.startsWith("LD|F") || linea.startsWith("LD|D")
                                        ) {
                                } else if (!linea.matches("LD|-")) {
                                    if (!linea.contains("Seq.No.") && !linea.contains("Documt.log")) {
                                        linea = linea.replace(",", "");
                                        linea = linea.replace("-", "");
                                        linea = linea.replace("|", "~");
                                        
                                        parts = linea.split("~");
                                        //System.out.println("limpiartxt.LeerFichero.main("+i+"): "+parts.toString());
                                        reg = new ArrayList();
                                        for (int j = 1; j < parts.length; j++  ){ 
                                            reg.add(parts[j]); 
                                            System.out.println("["+j+"]: "+parts[j]); 
                                        } 
                                        
                                        //System.out.println("\n "+reg.size());
                                        System.out.println("***********************************************");
                                        
                                        if (ub.get(i).toString().contains("RFCOREPOR_D1")) { 
                                            // CODIGO DE INSERCION EN TABLA DE SCRAP MISCELANEOS DÍA 
                                            if (!parts[9].isEmpty()){ //FORMATO DE FECHA, PARA QUE NO TRUENE EL CÓDIGO
                                                new dao.DAOScrap().insertaMicellaniousDay(reg); 
                                            }
                                            tipo = 1;
                                        } else if (ub.get(i).toString().contains("RFCOREPOR_D2")) { 
                                            // CODIGO DE INSERCION EN TABLA DE SCRAP IFK DÍA 
                                            if (!parts[8].isEmpty()){
                                                new dao.DAOScrap().insertaIFKDay(reg); 
                                            }
                                            
                                            tipo = 2;
                                        } else if (ub.get(i).toString().contains("RFCOREPOR_W1")) {   //SEMANA                                                                 
                                            /* EVALUAMOS PARA SABER SI YA SE ELIMINO LO DEL MES O SE TIENE QUE ELIMINAR  */
                                            if (bnRemove == 0) { 
                                                //ELIMINACION DE INFORMACION DEL MES 
                                                System.out.println("limpiartxt.LeerFichero.analisisArchivo().WM: "+parts[9].toString());
                                                //new dao.DAOScrap().deleteMicellaniousWeek(parts[9].toString());
                                                bnRemove = 1; 
                                            } 
                                            // CODIGO DE INSERCION EN TABLA DE SCRAP MISCELANEOS MES 
                                            new dao.DAOScrap().insertaMicellaniousDay(reg); 
                                            tipo = 3;
                                        } else if (ub.get(i).toString().contains("RFCOREPOR_W2")) { //SEMANA
                                            /* EVALUAMOS PARA SABER SI YA SE ELIMINO LO DEL MES O SE TIENE QUE ELIMINAR  */
                                            if (bnRemove == 0) { 
                                                //ELIMINACION DE INFORMACION DEL MES 
                                                System.out.println("limpiartxt.LeerFichero.analisisArchivo().WI: "+parts[8].toString());
                                                new dao.DAOScrap().deleteIFKWeek(parts[8].toString()); 
                                                bnRemove = 1; 
                                            } 
                                            // CODIGO DE INSERCION EN TABLA DE SCRAP IFK MES 
                                            new dao.DAOScrap().insertaIFKDay(reg); 
                                            tipo = 4; 
                                        } else if (ub.get(i).toString().contains("RFCOREPOR_M1")) {                                                                    
                                            /* EVALUAMOS PARA SABER SI YA SE ELIMINO LO DEL MES O SE TIENE QUE ELIMINAR  */
                                            if (bnRemove == 0) { 
                                                //ELIMINACION DE INFORMACION DEL MES 
                                                System.out.println("limpiartxt.LeerFichero.analisisArchivo().M: "+parts[9].toString());
                                                new dao.DAOScrap().deleteMicellaniousMonth(parts[9].toString());
                                                bnRemove = 1; 
                                            } 
                                            // CODIGO DE INSERCION EN TABLA DE SCRAP MISCELANEOS MES 
                                            new dao.DAOScrap().insertaMicellaniousDay(reg); 
                                            tipo = 3;
                                        } else if (ub.get(i).toString().contains("RFCOREPOR_M2")) { 
                                            /* EVALUAMOS PARA SABER SI YA SE ELIMINO LO DEL MES O SE TIENE QUE ELIMINAR  */
                                            if (bnRemove == 0) { 
                                                //ELIMINACION DE INFORMACION DEL MES 
                                                System.out.println("limpiartxt.LeerFichero.analisisArchivo().IFK: "+parts[8].toString());
                                                new dao.DAOScrap().deleteIFKMonth(parts[8].toString()); 
                                                bnRemove = 1; 
                                            } 
                                            // CODIGO DE INSERCION EN TABLA DE SCRAP IFK MES 
                                            new dao.DAOScrap().insertaIFKDay(reg); 
                                            tipo = 4;
                                        } 
                                    } 
                                } 
                            } 
                        } 
                    } 
                } catch (Exception e) { 
                    e.printStackTrace(); 
                } finally { 
                    try { 
                        if (null != fr) { 
                            fr.close(); 
                            String cadArchivo = archivo.toString(); 
                            String[] pArchivo = cadArchivo.split("\\\\"); 
                            System.out.println(" + limpiartxt.LeerFichero.analisisArchivo(): "+pArchivo[(pArchivo.length)-1]+": "+tipo);
                            
                            //System.out.println("limpiartxt.LeerFichero.analisisArchivo().Eliminar: "+archivo); 
                            //ELIMINA LOS REGISTROS DE 
                            new dao.DAOScrap().elimnaRegistroArchivo(tipo); 
                            //REGISTRA EL ARCHIVO UTILIZADO 
                            new dao.DAOScrap().registroArchivo(pArchivo[(pArchivo.length)-1], tipo); 
                            
                            // BORRA EL ARCHIVO :) 
                            archivo.delete(); 
                            analisisArchivo(); 
                        } 
                    } catch (Exception e2) { 
                        e2.printStackTrace(); 
                    } 
                } 
            } 
        } 
    } 
} 
